#!/bin/bash

# composer require --dev "bex/behat-screenshot":"^2.1" "dmore/behat-chrome-extension":"^1.3" "drupal/drupal-extension":"^4.1" "emuse/behat-html-formatter":"^0.2.0"


sed -i 's/localhost/apache/' behat_tests/behat.yml
sed -i 's/\/opt\/drupal\/web/\/var\/www\/html\/web/' behat_tests/behat.yml
sed -i 's/@javascript @api/@api/' behat_tests/features/*.feature




vendor/bin/behat --config behat_tests/behat.yml --init
# vendor/bin/behat --config behat_tests/behat.yml -dl --lang es
# vendor/bin/behat --config behat_tests/behat.yml -f progress
vendor/bin/behat --config behat_tests/behat.yml
# vendor/bin/behat --config behat_tests/behat.yml --help


sed -i 's/apache/localhost/' behat_tests/behat.yml
sed -i 's/\/var\/www\/html\/web/\/opt\/drupal\/web/' behat_tests/behat.yml
sed -i 's/@api/@javascript @api/' behat_tests/features/*.feature
