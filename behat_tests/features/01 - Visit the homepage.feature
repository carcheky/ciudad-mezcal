@javascript @api
Feature: Visit the homepage
  In order to test Drupal
  As an anonymous user
  I need to be able to see the homepage

  Scenario: Visit the homepage
    Given I visit "/"
    Then I should see "Ciudad Mezcal"
