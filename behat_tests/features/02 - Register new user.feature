@javascript @api
Feature: Register new user
 In order to register a new user
 I want to access /user/register/mezcalero
 So that I can register a new user
 Then I go to /
 So that I can login

Scenario: Register new user
Given I am an anonymous user
 When I go to "/user/register/mezcalero"
 And I enter "behat@test.test" for "edit-mail"
 And I enter "behat" for "edit-name"
 When I press "edit-submit"
 Then I go to "/"
 And I enter "behat" for "edit-name"
 When I press "edit-submit"
 Then I should see "You are now logged in as behat."
 Given I run drush "ucan behat -y --delete-content"